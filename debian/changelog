jpegpixi (1.1.1-5) unstable; urgency=medium

  * QA upload.
  * Migrations:
      - CDBS to DH.
      - Copyright to 1.0 format.
      - DebSrc to 3.0.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
      - debian/docs: added 'AUTHORS' to install documentation.
        Build-Depends field and bumped level to 13.
      - Closes: #965608
  * debian/clean: created to remove files generated from the build process.
  * debian/control:
      - Added Vcs-* field.
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.6.0.
      - Set Debian QA Group as maintainer. (see: #929538)
  * debian/patches/010_fix-groff-wrong-macro.patch: created to fix a macro
    error in a translation file.
  * debian/rules: added DEB_BUILD_MAINT_OPTIONS variable to improve GCC
    hardening.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/source/include-binaries: added to allow binarie in CI tests.
  * debian/source/options: created to ignore changes in po/.
  * debian/tests/control: added new tests.
  * debian/watch: updated using a fake site to explain about the current
    status of the original upstream homepage.

 -- Guilherme de Paula Xavier Segundo <guilherme.lnx@gmail.com>  Tue, 26 Apr 2022 09:32:15 -0300

jpegpixi (1.1.1-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * Build-Depend on libjpeg-dev instead of libjpeg62-dev (closes: #642985)

 -- Julien Cristau <jcristau@debian.org>  Sun, 29 Jan 2012 18:24:34 +0100

jpegpixi (1.1.1-4) unstable; urgency=low

  * debian/rules: Fixed FTBFS if build three times in a row (Closes: #441744).
    Thanks to Sandro Tosi <matrixhasu@gmail.com>.
  * debian/control:
    - Bumped Standards-Version to 3.7.3. No changes.
    - Moved Homepage reference.

 -- Kevin Coyner <kcoyner@debian.org>  Tue, 15 Apr 2008 16:21:00 -0400

jpegpixi (1.1.1-3) unstable; urgency=low

  * New maintainer. Closes: #425275.
  * Add colons to bug closes in debian/changelog per Policy.
  * debian/control:
    - Bumped debhelper version to 5.
    - Bumped Standards-Version to 3.7.2. No changes.
    - Added homepage URL.
    - Small formatting changes to description.
    - Added ${misc:Depends} to Depends.
  * Updated FSF address in debian/copyright. Small formatting changes.
  * Bumped debian/watch file to version 3.
  * Bumped debian/compat to 5.

 -- Kevin Coyner <kcoyner@debian.org>  Wed, 15 Aug 2007 15:21:29 -0400

jpegpixi (1.1.1-2) unstable; urgency=low

  * Orphaning the package.

 -- Jarno Elonen <elonen@debian.org>  Fri, 18 May 2007 12:18:43 +0300

jpegpixi (1.1.1-1) unstable; urgency=low

  * New upstream release (closes: #337181)

 -- Jarno Elonen <elonen@debian.org>  Mon, 21 Nov 2005 09:13:20 +0200

jpegpixi (1.1.0-1) unstable; urgency=low

  * New upstream release
    - Auto-detection of burned pixels in addition to dead ones.
  * Fixed watch file

 -- Jarno Elonen <elonen@debian.org>  Fri,  3 Jun 2005 13:17:26 +0300

jpegpixi (1.0.3-2) unstable; urgency=low

  * Fixed Build-Depends. (closes: #278405)

 -- Jarno Elonen <elonen@debian.org>  Wed, 27 Oct 2004 12:44:20 +0300

jpegpixi (1.0.3-1) unstable; urgency=low

  * New upstream release
    - French translations

 -- Jarno Elonen <elonen@debian.org>  Sat, 16 Oct 2004 20:10:05 +0300

jpegpixi (1.0.2-1) unstable; urgency=low

  * New upstream release

 -- Jarno Elonen <elonen@debian.org>  Sun, 10 Oct 2004 00:28:09 +0300

jpegpixi (1.0.1-1) unstable; urgency=low

  * New upstream release
    - Finnish translations (Kah, niin sitä pitää..)

 -- Jarno Elonen <elonen@debian.org>  Mon,  4 Oct 2004 22:30:37 +0300

jpegpixi (1.0.0-1) unstable; urgency=low

  * New upstream release

 -- Jarno Elonen <elonen@debian.org>  Sun,  3 Oct 2004 15:59:48 +0300

jpegpixi (0.16.0-1) unstable; urgency=low

  * New upstream release
    - German manpages (Ja! Gut..)

 -- Jarno Elonen <elonen@debian.org>  Wed, 25 Aug 2004 00:13:17 +0300

jpegpixi (0.15.1-1) unstable; urgency=low

  * New upstream release

 -- Jarno Elonen <elonen@debian.org>  Fri,  7 May 2004 09:20:26 +0300

jpegpixi (0.15.0-1) unstable; urgency=low

  * New upstream release

 -- Jarno Elonen <elonen@debian.org>  Tue, 20 Apr 2004 15:42:18 +0300

jpegpixi (0.14.2-1) unstable; urgency=low

  * New upstream release

 -- Jarno Elonen <elonen@debian.org>  Wed, 17 Mar 2004 08:57:31 +0200

jpegpixi (0.14.1-1) unstable; urgency=low

  * First upload to Main. (closes: #229716)
  * New upstream release.
    - debian/ directory removed (i.e. "moved" to Debian's .diff)
  * New package maintainer.

 -- Jarno Elonen <elonen@debian.org>  Mon, 26 Jan 2004 12:13:59 +0200

jpegpixi (0.14-1) unstable; urgency=low

  * New upstream version:
    - A new program `jpeghotp' is provided. It finds hot pixels in an
      otherwise black JPEG image.
    - Spelling mistakes in the manual page corrected.

 -- Martin Dickopp <martin@zero-based.org>  Sun, 11 Jan 2004 15:08:29 +0100

jpegpixi (0.13-1) unstable; urgency=low

  * New upstream version:
    - Coordinates and sizes can not only be specified as absolute values,
      but also as percentages of the image size.

 -- Martin Dickopp <martin@zero-based.org>  Mon, 24 Nov 2003 22:41:28 +0100

jpegpixi (0.12-1) unstable; urgency=low

  * New upstream version:
    - Uses the "opag" option parser generator.

 -- Martin Dickopp <martin@zero-based.org>  Sun, 16 Nov 2003 19:21:22 +0100

jpegpixi (0.11-2) unstable; urgency=low

  * Adapted debian/rules to current debhelper template.
  * Standards-Version: 3.6.0.

 -- Martin Dickopp <martin@zero-based.org>  Sat,  2 Aug 2003 02:33:17 +0200

jpegpixi (0.11-1) unstable; urgency=low

  * New upstream version.
  * Changed all occurrences of home page to <http://www.zero-based.org/software/jpegpixi/>.

 -- Martin Dickopp <martin@zero-based.org>  Tue, 29 Jul 2003 20:07:04 +0200

jpegpixi (0.10-2) unstable; urgency=low

  * Changed all occurrences of my e-mail address to <martin@zero-based.org>.

 -- Martin Dickopp <martin@zero-based.org>  Tue, 29 Jul 2003 16:41:19 +0200

jpegpixi (0.10-1) unstable; urgency=low

  * New upstream version.

 -- Martin Dickopp <mdickopp@users.sourceforge.net>  Sat,  5 Jul 2003 22:48:58 +0200

jpegpixi (0.9-1) unstable; urgency=low

  * New upstream version.

 -- Martin Dickopp <mdickopp@users.sourceforge.net>  Sun, 22 Jun 2003 18:40:59 +0200

jpegpixi (0.8-1) unstable; urgency=low

  * New upstream version.

 -- Martin Dickopp <mdickopp@users.sourceforge.net>  Sun, 22 Jun 2003 00:29:41 +0200

jpegpixi (0.7-1) unstable; urgency=low

  * New upstream version.
  * Standards-Version: 3.5.10.
  * Convert to debhelper 4.

 -- Martin Dickopp <mdickopp@users.sourceforge.net>  Mon,  2 Jun 2003 11:16:21 +0200

jpegpixi (0.6-2) unstable; urgency=low

  * Use debhelper instead of debmake.

 -- Martin Dickopp <mdickopp@users.sourceforge.net>  Sun, 16 Mar 2003 13:07:02 +0100

jpegpixi (0.6-1) unstable; urgency=low

  * Initial Debian release (Martin's unofficial repository).

 -- Martin Dickopp <mdickopp@users.sourceforge.net>  Sat, 11 Jan 2003 01:00:05 +0100
